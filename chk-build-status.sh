#!/bin/bash

count=0     
while true
do
	uuid=$( sudo composer-cli compose status | awk '{print $1}')
        status=$( sudo composer-cli compose status | grep ${uuid} | awk '{print $2}')

        if [ $status == "FINISHED" ];
        then
            echo -e "${uuid} has been finished"
            break
        else
            if [ $count -eq 0 ];
            then
                echo -n "wait for image build: $uuid"
            else
                echo -n "."
            fi
            count=`expr $count + 1`
            continue
        fi
done
