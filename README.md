# MicroShiftインストール済みのRHEL for Edgeイメージの作成

本リポジトリでは、MicroShiftインストール済みのRHEL for Edgeイメージを作成する方法を学びます。

> Note.
>  RHEL for Edgeは、RHELの中のエッジ向け機能にフィーチャーしたブランドを表し、RHEL for Edgeという名称のプロダクトはありません。
>  RHEL for EdgeとRHELは同一プロダクトです。

> 参考
> [ MicroShift 製品ドキュメント](https://access.redhat.com/documentation/en-us/red_hat_build_of_microshift)

# 前提条件

## Pull Secretの取得
以下のサイトにアクセスし、pull-secretを取得しておいてください。(新規の場合はアカウント作成が必要です)
https://cloud.redhat.com/openshift/install/pull-secret

## 本リポジトリをclone

```bash
cd rhel-image-build/
```

1-1. MicroShiftが含まれるローカルのyumリポジトリの構築
===

# MicroShiftのRPMパッケージを取得
それでは、RHEL for EdgeのOSイメージを作成してみましょう。
最初にMicroShiftのインストールに使用するリポジトリをローカルに構築します。

レッドハットのリポジトリからローカルへシンクしましょう。

```bash
mkdir -p /var/repos/microshift-local
reposync --arch=$(uname -i) \
--arch=noarch --gpgcheck \
--download-path /var/repos/microshift-local \
--repo=rhocp-4.13-for-rhel-9-x86_64-rpms \
--repo=fast-datapath-for-rhel-9-x86_64-rpms
```

シンクしたら、以下のコマンドを実行し、取得したrpmパッケージが存在するか確認します。

```bash
find /var/repos/microshift-local -name \*coreos\* -print -exec rm -f {} \;
```

```
[実行結果]
/var/repos/microshift-local/rhocp-4.13-for-rhel-9-x86_64-rpms/Packages/c/coreos-installer-bootinfra-0.17.0-1.rhaos4.13.el9.x86_64.rpm
/var/repos/microshift-local/rhocp-4.13-for-rhel-9-x86_64-rpms/Packages/c/coreos-installer-dracut-0.17.0-1.rhaos4.13.el9.x86_64.rpm
/var/repos/microshift-local/rhocp-4.13-for-rhel-9-x86_64-rpms/Packages/c/coreos-installer-0.17.0-1.rhaos4.13.el9.x86_64.rpm
```

# ローカルにリポジトリのミラーサーバを作成

シンク先の`/var/repos/microshift-local`をリポジトリ化します。

```bash
createrepo /var/repos/microshift-local
```

# リポジトリのソースを定義
演習用コンテンツの`source/microshift.toml`を`/var/repos/microshift-local/microshift.toml`へコピーしてください。

```bash
cp -p source/microshift.toml /var/repos/microshift-local/microshift.toml
```

`microshift.toml`の定義内容は以下のとおりです。

```
id = "microshift-local"
name = "MicroShift local repo"
type = "yum-baseurl"
url = "file:///var/repos/microshift-local/"
check_gpg = false
check_ssl = false
system = false
```

# ローカルリポジトリをImage Builderに設定

`composer-cli sources`コマンドを実行し、ローカルのリポジトリを指定します。

```bash
composer-cli sources add /var/repos/microshift-local/microshift.toml
composer-cli sources list
```

以下の結果が表示されれば正常です。

```
[実行結果]
appstream
baseos
microshift-local
```


1-2. Blueprintの作成
===

次に、OSのBlueprintを定義します。

# Blueprintの定義ファイル(toml)を作成

演習用のコンテンツの`blueprints/rhde-microshift.toml `をホームディレクトリ配下へコピーしてください。

```bash
cp -p blueprints/rhde-microshift.toml ~/
```

Blueprintの定義内容は以下のとおりです。インストールするパッケージを定義します。

```bash
name = "rhde-microshift"
description = "RHDE Microshift Image"
version = "1.0.0"
modules = []
groups = []

[[packages]]
name = "microshift"
version = "*"

[[packages]]
name = "avahi"
version = "*"

[[packages]]
name = "openshift-clients"
version = "*"

[[packages]]
name = "git"
version = "*"

[[packages]]
name = "iputils"
version = "*"

[[packages]]
name = "bind-utils"
version = "*"

[[packages]]
name = "net-tools"
version = "*"

[[packages]]
name = "iotop"
version = "*"

[[packages]]
name = "redhat-release"
version = "*"

[customizations]

[customizations.services]
enabled = ["microshift"]
```

# BlueprintをImage Builderへプッシュ

Image BuilderへBlueprintを追加します。

```bash
composer-cli blueprints push ~/rhde-microshift.toml
composer-cli blueprints list
```

以下の結果が表示されれば正常です。

```bash
[実行結果]
rhde-microshift
```

1-3. OSイメージのビルド
===

Blueprintを用いて、OSイメージをビルドします。
以下のコマンドを実行してください。

```bash
composer-cli compose start-ostree rhde-microshift rhel-edge-container
```

```bash
[実行結果]
Compose d5d57d57-8da5-487f-81a5-162691d2e912 added to the queue
```

ビルドの状況は以下のコマンドで確認できます。

```bash
composer-cli compose status
```

```bash
[実行結果]
d5d57d57-8da5-487f-81a5-162691d2e912 RUNNING  Sat Feb 4 14:03:28 2023 rhde-microshift 1.0.0 edge-container
```

出力結果のステータスが`FINISHED`となるまで、待ちましょう。
> 15-30分程度時間がかかる場合があります。Runningの状態の場合は気長に待ちましょう。

```bash
[実行結果]
d5d57d57-8da5-487f-81a5-162691d2e912 FINISHED  Sat Feb 4 14:03:28 2023 rhde-microshift 1.0.0 edge-container
```


1-4. コンテナイメージを出力
===

作成されたOSイメージをコンテナイメージとして出力します。

```bash
uuid=$( sudo composer-cli compose status | awk '{print $1}' | grep -v ID | head -n 1)
composer-cli compose image $uuid
```

# 作成したコンテナからリポジトリの構成ファイルを取得

出力したコンテナイメージを用いてコンテナを起動し、リポジトリの構成ファイルをローカルにコピーしてください。
コンテナイメージを`skopeo`を用いてローカルのコンテナレジストリへコピーします。

```bash
skopeo copy oci-archive:${uuid}-container.tar containers-storage:localhost/rhde-microshift:latest
```

次に、Podmanでコンテナを起動します。

```bash
podman run --rm -p 8000:8080 rhde-microshift:latest &
podname=$(sudo podman ps | grep -v IMAGE | awk '{print $1}')
```

起動したコンテナの`/usr/share/nginx/html/repo`がリポジトリのパスです。
ローカルへコンテンツをコピーしましょう。

```bash
mkdir -p ~/generate-iso/ostree
podman cp ${podname}:/usr/share/nginx/html/repo ~/generate-iso/ostree
```

コピーが終了したら、以下のコマンドでコンテナを停止します。

```bash
podman stop ${podname}
```

1-5. ISOの作成
===

Red Hat のダウンロードサイトからダウンロードした`rhel-9.2-x86_64-boot.iso`をベースに、Kickstartを含めたISOを作成します。
演習用ディレクトリの`generate-iso`配下のGrup、ブートメニュー、Kickstartの設定ファイルをホームディレクトリの`generate-iso`配下へコピーします。

```bash
mkdir -p ~/results
cp -p generate-iso/* ~/generate-iso/
mv ~/rhel-9.2-x86_64-boot.iso ~/generate-iso/
```

合わせて、ISOを作成するためのコマンドをまとめたシェルスクリプトである `recook.sh`へ実行権限を付与しておきましょう。

```bash
chmod 755 ~/generate-iso/recook.sh
```

# Kickstartの修正

Kickstartファイルの以下の箇所を修正して、自身のPull Secretの内容へ置き換えてください。

```bash
vi ~/generate-iso/ks.cfg
```

```bash
[修正箇所]
...
mkdir -p /etc/crio
cat > /etc/crio/openshift-pull-secret << PULLSECRETEOF
REPLACE_HERE_TO_YOUR_OWN_PULL_SECRET --> Pull Secretの内容へ置き換えます
PULLSECRETEOF
...
```

# ISO作成

`recook.sh`を実行して、ISOを作成しましょう。

```bash
~/generate-iso/recook.sh
```

正常に終了すれば、以下の結果が出力され、MicroShiftインストール済みのOSイメージを作成できているはずです。

```bash
[実行結果]
...
xorriso : UPDATE :  94.36% done, estimate finish Wed May 31 10:32:14 2023
xorriso : UPDATE :  95.72% done
xorriso : UPDATE :  97.59% done
xorriso : UPDATE :  99.42% done
ISO image produced: 1020256 sectors
Written to medium : 1020256 sectors at LBA 0
Writing to 'stdio:../rhde-ztp.iso' completed successfully.
...
~/generate-iso
+ mv /tmp/tmp.3Lm4YeH4EL/new/rhde-ztp.iso ./
+ rm -rf /tmp/tmp.3Lm4YeH4EL
++ stat -c %U:%G .
+ chown root:root ./rhde-ztp.iso
```

```bash
ls -ltr ~/generate-iso/rhde-ztp.iso
```
