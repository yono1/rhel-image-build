#!/bin/bash

# check the creating image status
function chk_build_status {
    count=0
    while true
    do
        uuid=$( sudo composer-cli compose status | awk '{print $1}' | grep -v ID | head -n 1)
        status=$( sudo composer-cli compose status | grep ${uuid} | awk '{print $2}' | head -n 1)

        if [ "$status" == "FINISHED" ];
        then
            echo -e "${uuid} has been finished"
            break
        else
            count=`expr $count + 1`
	    echo -n "."
            continue
        fi
    done
}

# Prerequisites
## registration subscription
subscription-manager register \
--username=${USERNAME} \
--password=${PASSWORD}

## repository enabled
sudo subscription-manager repos \
--enable rhocp-4.13-for-rhel-9-x86_64-rpms \
--enable fast-datapath-for-rhel-9-x86_64-rpms

## package install
sudo dnf -y install \
createrepo yum-utils lorax skopeo \
composer-cli cockpit-composer podman genisoimage syslinux isomd5sum avahi

## Run Image Builder
sudo systemctl enable --now cockpit.socket
sudo systemctl enable --now osbuild-composer.socket

# Prepare Source Repositry
## reposync from Red Hat Repo to local
sudo mkdir -p /var/repos/microshift-local
sudo reposync --arch=$(uname -i) \
--arch=noarch --gpgcheck \
--download-path /var/repos/microshift-local \
--repo=rhocp-4.13-for-rhel-9-x86_64-rpms \
--repo=fast-datapath-for-rhel-9-x86_64-rpms

sudo find /var/repos/microshift-local -name \*coreos\* -print -exec rm -f {} \;

## Create Repo
sudo createrepo /var/repos/microshift-local
sudo cp -p source/microshift.toml /var/repos/microshift-local/microshift.toml

## Add Repo Source
sudo composer-cli sources add /var/repos/microshift-local/microshift.toml
sudo composer-cli sources list

# Prepare Blueprint
sudo cp -p blueprints/rhde-microshift.toml ~/
sudo composer-cli blueprints push ~/rhde-microshift.toml
sudo composer-cli blueprints list

# Create RHDE image
sudo composer-cli compose start-ostree rhde-microshift rhel-edge-container

chk_build_status

## output container image
sudo composer-cli compose image $uuid

# Get repo contents from a created container
sudo skopeo copy oci-archive:${uuid}-container.tar containers-storage:localhost/rhde-microshift:latest
sudo podman run --rm -p 8000:8080 rhde-microshift:latest &
podname=$(sudo podman ps | grep -v IMAGE | awk '{print $1}')

mkdir -p ~/generate-iso/ostree
sudo podman cp ${podname}:/usr/share/nginx/html/repo ~/generate-iso/ostree
sudo podman stop ${podname}

# create ISO (w/ kickstart)
mkdir -p ~/results
cp -p generate-iso/* ~/generate-iso/
cp -p iso/rhel-9.2-x86_64-boot.iso ~/generate-iso/
chmod 755 ~/generate-iso/recook.sh
~/generate-iso/recook.sh

mv rhde-ztp.iso ~/results/
ls -ltr ~/results/

# Create RHEL for Edge VM ( 4CPU / 8GB RAM / 20GB DISK)
sudo -b bash -c " \
cd /var/lib/libvirt/images && \
virt-install \
    --name rhde \
    --cpu host \
    --boot hd,cdrom \
    --vcpus sockets=1,cores=4,threads=1 \
    --memory 4096 \
    --disk path=./rhde.qcow2,size=10 \
    --network network=default,model=virtio \
    --events on_reboot=restart \
    --cdrom /var/lib/libvirt/images/rhde-ztp.iso \
    --graphics vnc,port=5901,listen=0.0.0.0 \
    --noautoconsole \
    --check disk_size=off \
    --quiet
"